<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8"
    <title>Chessboard</title>
</head>
<style>
    .main {
        width: 400px;
        margin: 0;
        padding: 0;
    }
    .black,.red{
        width: 100px;
        width: 50px;
        height: 50px;
        display: inline-block;
        margin: 0;
        padding: 0;
        background-color: #111;
        vertical-align: bottom;

    }
    .red {
        background-color: white;
    }
</style>
<body>
<div class="main">
    <?php
    for($i = 1; $i <= 8 ; $i++){
        for($j = 1; $j <= 8; $j++){
            if($i%2 == 0) {
                if($j%2 == 0){
                    echo "<div class='black' > </div>";
                }else{
                    echo "<div class='red' > </div>";
                }
            }else{
                if($j%2 == 0){
                    echo "<div class='red' > </div>";
                }else{
                    echo "<div class='black' > </div>";
                }
            }
        }
    }
    ?>
</div>
</body>
</html>
